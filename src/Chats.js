import React from 'react'
import Chat from './Chat'

const Chats = ()=> {
    return (
        <div className="chats">
            <Chat
            name="Mark"
            message="Whats up!"
            timestamp="40 seconds ago"
            profilePic="...."
         /> 
         <Chat
            name="Sarah"
            message="Hey! How are you"
            timestamp="35 minutes ago"
            profilePic="...."
         /> 
         <Chat
            name="Ellen"
            message="Whats up?"
            timestamp="55 minutes ago"
            profilePic="...."
         /> 
         <Chat
            name="Sandra"
            message="Ola!"
            timestamp="3 Days ago"
            profilePic="...."
         /> 
         <Chat
            name="Sandra"
            message="Oops there is he ..."
            timestamp="1 week ago"
            profilePic="...."
         /> 
         
        </div>
    )
}

export default Chats
