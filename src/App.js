import React from 'react';
import Header from './Header'
import TinderCards from './TinderCards'
import SwipeButtons from './SwipeButtons'
import Chats from './Chats'
import ChatScreen from './ChatScreen'
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {
  return (
    <div className="App">
     
      <Router>
      <Switch>
        <Route path="/chat/:person">
         {/* Header */}
         <Header backButton="/chat" />
          <ChatScreen/>
      </Route>
        <Route path="/chat">
         {/* Header */}
         <Header backButton="/" />
          <Chats/>
      </Route>
        <Route path="/">
         {/* Header */}
         <Header />  
{/* Tinder card */}
<TinderCards/>
{/* Button below tinder card */}
<SwipeButtons/>
{/* Chats Screen */}
{/* Individual Chats Screen */}
        </Route>
      </Switch>
    </Router>
    </div>
  );
}

export default App;





