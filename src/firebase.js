import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyDszj3W8jwM-3sn2mDzmyCyRjQgvMdnJ90",
    authDomain: "tender-clone-12353.firebaseapp.com",
    databaseURL: "https://tender-clone-12353.firebaseio.com",
    projectId: "tender-clone-12353",
    storageBucket: "tender-clone-12353.appspot.com",
    messagingSenderId: "903287677277",
    appId: "1:903287677277:web:cb59642b861af858a288b4",
    measurementId: "G-EHGSLVQDBY"
  };

const firebaseApp = firebase.initializeApp(firebaseConfig);
const database =  firebaseApp.firestore();

export default database;
