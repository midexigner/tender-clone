import React, { useState } from 'react'
import './ChatScreen.css';
import Avatar from '@material-ui/core/Avatar';

const ChatScreen = ()=> {
    const [input,setInput] = useState('');
const [message,setMessage] = useState([
{
    name:"Ellen",
    image:'..',
    message:"whats Up"
},
{
    name:"Ellen",
    image:'..',
    message:"how it going"
},
{
    
    message:"how it going"
}
]);
const handleSend = (e)=>{
   e.preventDefault();
   setMessage([...message,{message:input}]);
   setInput("");
}
    return (
        <div className="chatScreen">
            <p className="chatScreen__timestamp">YOU MATCHED ELLEN on 10/08/20</p>
            {message.map((message)=>(
                message.name ? (
<div className="chatScreen__message">
                      <Avatar 
                      className="chatScreen__image" 
                      alt={message.name} 
                      src={message.image}
                       />  
                    <p className="chatScreen__text">{message.message}</p>
                </div>
                ):(
                    <div className="chatScreen__message">
                    <p className="chatScreen__textUser">{message.message}</p>
                </div>
                )
                
            ))}

            
                <form className="chatScreen__input" >
                    <input
                    value={input}
                    onChange={(e)=>setInput(e.target.value)}
                    className="chatScreen__inputField" 
                    placeholder=" type a message"
                    type="text"
                    />
                    <button
                    onClick={handleSend}
                    type="submit" className="chatScreen__inputButton" >SEND</button>
                </form>
            
        </div>
    )
}

export default ChatScreen
